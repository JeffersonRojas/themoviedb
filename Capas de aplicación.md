# Capas de aplicación

## 1. Activities
Este package contiene todas las activities de la app
### 1.1 Buscar
Buscar se encarga de enviar el query de busqueda a los fragments de Peliculas y Series
### 1.2 Detalle Item
Detalle Item muestra en detalle una serie o pelicula seleccionada
### 1.3 Peliculas
Peliculas se encarga de cargar los fragments de peliculas en las secciones Popular, Top Rated y Up Coming
### 1.4 Series
Series se encarga de cargar los fragments de series en las secciones Popular y Top Rated
### 1.5 Splash
Splash se encarga de mostrar un splash cuando se abre la aplicación
## 2. Adapters
Este package contiene los adaptadores de los ViewPagers y RecyclerViews
### 2.1 PagerAdapterList
PagerAdpterList es el adaptador del ViewPager que contiene la lista de peliculas o series
### 2.2 PagerAdapterSearch
PagerAdapterSearch es el adaptador del ViewPager de la activity Buscar
### 2.3 RecyclerAdapter
RecyclerAdapter es el adaptador del RecyclerView de los fragments RecyclerFragmentList y RecyclerFragmentSearc, se encarga de cargar los items de peliculas o series.
## 3. Base
Este package contiene las clases padres de las activities y de la clase Application
### 3.1 BaseDrawer
BaseDrawer se encarga de gestionar el DrawerMenu de las activities que contengan uno
### 3.2 MyApplication
MyApplication se encarga de ejecutar metodos de inicialización de librerias cuando se carga la app
## 4. Fragments
Este package contiene los fragments y viewholders de la aplicación
### 4.1 ItemViewHolder
ItemViewHolder se encarga de cargar el modelo de pelicula o serie en la vista
### 4.2 RecyclerFragmentList
RecyclerFragmentList se encarga de cargar la lista de peliculas o series solo en las activities Peliculas y Series
### 4.3 RecyclerFragmentSearch
RecyclerFragmentSearch se encarga de cargar la lista de peliculas o series solo en la activity Buscar, tambien recibe el query de la busqueda para fitrar la lista
## 5. Models
Este package contiene los DTOS de la aplicación
### 5.1 DetallesPelicula
Contiene informacion adicional de una pelicula que PeliculaModel
### 5.2 ListaPeliculas
Contiene el resultado del servicio que retorna peliculas
### 5.3 ListaSeries
Contiene el resultado del servicio que retorna series
### 5.4 PeliculaModel
Contiene los datos basicos de una pelicula
### 5.5 SerieModel
Contiene los datos basicos de una serie
## 6. Request
Este package contiene los metodos y clases de Retrofit
### 6.1 ApiClient
ApiClient se encarga de retornar el cliente retrofit
### 6.2 ApiInterface
ApiInterface contiene los metodos retrofit para consumir servicios
### 6.3 ApiResponse
ApiResponse se encarga de gestionar la respuesta de los servicios
## 7. Utils
Este package contiene metodos que se pueden requerir en cualquier parte de la aplicación
### 7.1 DataBindingUtils
DataBindingUtils contiene metodos que seran utilizados por la libreria de DataBinding
### 7.2 MyScrollListener
MyScrollListener es un Listener de los RecyclerView que se encarga de informar que ha llegado al final de la lista
### 7.3 SugarUtils
SugarUtils contiene todos los metodos de conexión, consulta y actualización a la base de datos SQLITE usando la libreria Sugar

