package org.themoviedb.activities;


import android.content.Context;
import android.support.annotation.IdRes;
import android.support.annotation.StringRes;
import android.support.test.InstrumentationRegistry;
import android.support.test.espresso.ViewInteraction;
import android.support.test.filters.LargeTest;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.view.KeyEvent;
import android.widget.EditText;

import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.themoviedb.R;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.pressKey;
import static android.support.test.espresso.action.ViewActions.swipeDown;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.action.ViewActions.typeText;
import static android.support.test.espresso.matcher.ViewMatchers.isAssignableFrom;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withContentDescription;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class AppUITest {

    @Rule
    public ActivityTestRule<Peliculas> mActivityTestRule = new ActivityTestRule<>(Peliculas.class);

    @Test
    public void splashTest() {

        clickOnTabWithdText(R.string.popular);

        testLoadItems(R.id.drawer_layout);

        clickOnTabWithdText(R.string.toprated);

        testLoadItems(R.id.drawer_layout);

        clickOnTabWithdText(R.string.upcoming);

        testLoadItems(R.id.drawer_layout);

        openDrawerLayout();

        clickMenuItem(R.string.series);

        clickOnTabWithdText(R.string.popular);

        testLoadItems(R.id.drawer_layout);

        clickOnTabWithdText(R.string.toprated);

        testLoadItems(R.id.drawer_layout);

        openDrawerLayout();

        clickMenuItem(R.string.buscar);

        onView(withId(R.id.action_search)).perform(click());

        onView(isAssignableFrom(EditText.class)).perform(typeText("a"), pressKey(KeyEvent.KEYCODE_ENTER));

        clickOnTabWithdText(R.string.peliculas);

        testLoadItems(R.id.drawer_layout);

        clickOnTabWithdText(R.string.series);

        testLoadItems(R.id.drawer_layout);

    }

    private void testLoadItems(@IdRes int drawer_id) {
        onView(withId(drawer_id)).perform(swipeDown());
        for (int i = 0 ; i < 5 ; i++) {
            onView(withId(drawer_id)).perform(swipeUp());
        }
        onView(withId(drawer_id)).perform(swipeDown());
    }

    private void clickOnTabWithdText(@StringRes int string_id) {
        onView(allOf(withText(getResourceString(string_id)), isDisplayed())).perform(click());
    }

    private void openDrawerLayout() {
        onView(withContentDescription(getResourceString(R.string.navigation_drawer_open))).perform(click());
    }

    private void clickMenuItem(@StringRes int string_id) {
        ViewInteraction appCompatCheckedTextView = onView(
                allOf(withId(R.id.design_menu_item_text), withText(getResourceString(string_id)), isDisplayed()));
        appCompatCheckedTextView.perform(click());
    }

    private String getResourceString(@StringRes int id) {
        Context targetContext = InstrumentationRegistry.getTargetContext();
        return targetContext.getResources().getString(id);
    }

}
