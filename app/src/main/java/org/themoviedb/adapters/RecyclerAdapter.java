package org.themoviedb.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.themoviedb.R;
import org.themoviedb.databinding.HolderPeliculaBinding;
import org.themoviedb.databinding.HolderSerieBinding;
import org.themoviedb.fragments.ItemViewHolder;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;

import java.util.List;

/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {

    private List<PeliculaModel> listPeliculas;
    private List<SerieModel> listSeries;

    public void setListPeliculas(List<PeliculaModel> listPeliculas) {
        this.listPeliculas = listPeliculas;
        listSeries = null;
    }

    public void setListSeries(List<SerieModel> listSeries) {
        this.listSeries = listSeries;
        listPeliculas = null;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if (listPeliculas != null) {
            Context context = parent.getContext();
            View view = LayoutInflater.from(context).inflate(R.layout.holder_pelicula, parent, false);
            return new ItemViewHolder(HolderPeliculaBinding.bind(view));
        } else {
            Context context = parent.getContext();
            View view = LayoutInflater.from(context).inflate(R.layout.holder_serie, parent, false);
            return new ItemViewHolder(HolderSerieBinding.bind(view));
        }
    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder viewHolder, int position) {
        if (listPeliculas != null) {
            ItemViewHolder holder = (ItemViewHolder) viewHolder;
            holder.setModel(listPeliculas.get(position));
        } else if (listSeries != null){
            ItemViewHolder holder = (ItemViewHolder) viewHolder;
            holder.setModel(listSeries.get(position));
        }
    }

    @Override
    public int getItemCount() {
        if (listPeliculas != null) {
            return listPeliculas.size();
        } else if (listSeries != null){
            return listSeries.size();
        } else {
            return 0;
        }
    }

}
