package org.themoviedb.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.themoviedb.fragments.RecyclerFragmentList;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class PagerAdapterList extends FragmentPagerAdapter {

    private final List<RecyclerFragmentList> fragmentList = new ArrayList<>();
    private final List<String> fragmentTitleList = new ArrayList<>();

    public PagerAdapterList(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(RecyclerFragmentList fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

}