package org.themoviedb.adapters;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

import org.themoviedb.fragments.RecyclerFragmentSearch;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jefferson Rojas on 21/03/2017.
 */

public class PagerAdapterSearch extends FragmentPagerAdapter {

    private final List<RecyclerFragmentSearch> fragmentList = new ArrayList<>();
    private final List<String> fragmentTitleList = new ArrayList<>();

    public PagerAdapterSearch(FragmentManager fragmentManager) {
        super(fragmentManager);
    }

    public void addFragment(RecyclerFragmentSearch fragment, String title) {
        fragmentList.add(fragment);
        fragmentTitleList.add(title);
    }

    public List<RecyclerFragmentSearch> getFragmentList() {
        return fragmentList;
    }

    @Override
    public Fragment getItem(int position) {
        return fragmentList.get(position);
    }

    @Override
    public int getCount() {
        return fragmentList.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return fragmentTitleList.get(position);
    }

}