package org.themoviedb.models;

import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class ListaPeliculas {

    @SerializedName("page")
    public int page;
    @SerializedName("results")
    public List<PeliculaModel> results;
    @SerializedName("total_results")
    public int total_results;
    @SerializedName("total_pages")
    public int total_pages;

}
