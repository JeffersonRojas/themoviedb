package org.themoviedb.models;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Jefferson Rojas on 23/03/2017.
 */

public class DetallesPelicula {

    @SerializedName("budget")
    public int budget;
    @SerializedName("id")
    public int id;
    @SerializedName("revenue")
    public int revenue;
    @SerializedName("runtime")
    public int runtime;

}
