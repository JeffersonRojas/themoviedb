package org.themoviedb.models;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;

import java.util.List;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

@Table
public class SerieModel {

    @SerializedName("poster_path")
    public String poster_path;
    @SerializedName("popularity")
    public double popularity;
    @SerializedName("id")
    public Long id;
    @SerializedName("backdrop_path")
    public String backdrop_path;
    @SerializedName("vote_average")
    public double vote_average;
    @SerializedName("overview")
    public String overview;
    @SerializedName("first_air_date")
    public String first_air_date;
    @SerializedName("origin_country") @Ignore
    public List<String> origin_country;
    @SerializedName("genre_ids") @Ignore
    public List<Integer> genre_ids;
    @SerializedName("original_language")
    public String original_language;
    @SerializedName("vote_count")
    public int vote_count;
    @SerializedName("name")
    public String name;
    @SerializedName("original_name")
    public String original_name;

    public String requestType;

    public boolean flagPopular;
    public boolean flagTopRated;

}
