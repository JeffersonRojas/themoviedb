package org.themoviedb.models;

import com.google.gson.annotations.SerializedName;
import com.orm.dsl.Ignore;
import com.orm.dsl.Table;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

@Table
public class PeliculaModel {

    @SerializedName("poster_path")
    public String poster_path;
    @SerializedName("adult")
    public boolean adult;
    @SerializedName("overview")
    public String overview;
    @SerializedName("release_date")
    public String release_date;
    @SerializedName("genre_ids") @Ignore
    public List<Integer> genre_ids = new ArrayList<>();
    @SerializedName("id")
    public Long id;
    @SerializedName("original_title")
    public String original_title;
    @SerializedName("original_language")
    public String original_language;
    @SerializedName("title")
    public String title;
    @SerializedName("backdrop_path")
    public String backdrop_path;
    @SerializedName("popularity")
    public double popularity;
    @SerializedName("vote_count")
    public int vote_count;
    @SerializedName("video")
    public boolean video;
    @SerializedName("vote_average")
    public double vote_average;

    public boolean flagPopular;
    public boolean flagTopRated;
    public boolean flagUpComing;

}
