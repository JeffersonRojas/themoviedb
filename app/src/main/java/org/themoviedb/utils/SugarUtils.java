package org.themoviedb.utils;

import com.orm.SugarRecord;
import com.orm.query.Select;

import org.themoviedb.models.ListaPeliculas;
import org.themoviedb.models.ListaSeries;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;

import java.util.ArrayList;
import java.util.List;

import static org.themoviedb.fragments.RecyclerFragmentList.REQUEST_MOVIE_POPULAR;
import static org.themoviedb.fragments.RecyclerFragmentList.REQUEST_MOVIE_TOP_RATED;
import static org.themoviedb.fragments.RecyclerFragmentList.REQUEST_MOVIE_UP_COMING;
import static org.themoviedb.fragments.RecyclerFragmentList.REQUEST_SERIES_POPULAR;
import static org.themoviedb.fragments.RecyclerFragmentList.REQUEST_SERIES_TOP_RATED;

/**
 * Created by Jefferson Rojas on 20/03/2017.
 */

public class SugarUtils {

    public static void savePeliculas(ListaPeliculas listaPeliculas) {
        for (PeliculaModel peliculamodel : listaPeliculas.results) {
            PeliculaModel peliculaModelDB = SugarRecord.findById(PeliculaModel.class,peliculamodel.id);
            if (peliculaModelDB != null) {
                peliculamodel.flagPopular = peliculaModelDB.flagPopular;
                peliculamodel.flagTopRated = peliculaModelDB.flagTopRated;
                peliculamodel.flagUpComing = peliculaModelDB.flagUpComing;
            }
        }
        SugarRecord.saveInTx(listaPeliculas.results);
    }

    public static void savePeliculas(String requestType, ListaPeliculas listaPeliculas) {
        if (listaPeliculas.page == 1) {
            List<PeliculaModel> peliculas;
            switch (requestType) {
                case REQUEST_MOVIE_POPULAR:
                    SugarRecord.deleteAll(PeliculaModel.class,"FLAG_POPULAR = ? AND FLAG_TOP_RATED = ? AND FLAG_UP_COMING = ?","1","0","0");
                    peliculas = getMoviesPopular();
                    if (peliculas != null) {
                        for (PeliculaModel peliculaModel: peliculas) {
                            peliculaModel.flagPopular = false;
                        }
                        SugarRecord.saveInTx(peliculas);
                    }
                    break;
                case REQUEST_MOVIE_TOP_RATED:
                    SugarRecord.deleteAll(PeliculaModel.class,"FLAG_POPULAR = ? AND FLAG_TOP_RATED = ? AND FLAG_UP_COMING = ?","0","1","0");
                    peliculas = getMoviesTopRated();
                    if (peliculas != null) {
                        for (PeliculaModel peliculaModel: peliculas) {
                            peliculaModel.flagTopRated = false;
                        }
                        SugarRecord.saveInTx(peliculas);
                    }
                    break;
                case REQUEST_MOVIE_UP_COMING:
                    SugarRecord.deleteAll(PeliculaModel.class,"FLAG_POPULAR = ? AND FLAG_TOP_RATED = ? AND FLAG_UP_COMING = ?","0","0","1");
                    peliculas = getMoviesUpComing();
                    if (peliculas != null) {
                        for (PeliculaModel peliculaModel: peliculas) {
                            peliculaModel.flagUpComing = false;
                        }
                        SugarRecord.saveInTx(peliculas);
                    }
                    break;
            }
        }
        boolean flagPupular = false;
        boolean flagTopRated = false;
        boolean flagUpComing = false;
        switch (requestType) {
            case REQUEST_MOVIE_POPULAR:
                flagPupular = true;
                break;
            case REQUEST_MOVIE_TOP_RATED:
                flagTopRated = true;
                break;
            case REQUEST_MOVIE_UP_COMING:
                flagUpComing = true;
                break;
        }
        for (PeliculaModel peliculamodel : listaPeliculas.results) {
            PeliculaModel peliculaModelDB = SugarRecord.findById(PeliculaModel.class,peliculamodel.id);
            if (peliculaModelDB != null) {
                peliculamodel.flagPopular = flagPupular || peliculaModelDB.flagPopular;
                peliculamodel.flagTopRated = flagTopRated || peliculaModelDB.flagTopRated;
                peliculamodel.flagUpComing = flagUpComing || peliculaModelDB.flagUpComing;
            } else {
                peliculamodel.flagPopular = flagPupular;
                peliculamodel.flagTopRated = flagTopRated;
                peliculamodel.flagUpComing = flagUpComing;
            }
        }
        SugarRecord.saveInTx(listaPeliculas.results);
    }

    public static List<PeliculaModel> searchPeliculas(String query) {
        return Select.from(PeliculaModel.class).where("TITLE LIKE ?",new String[]{"%"+query+"%"}).list();
    }

    public static List<PeliculaModel> getMoviesPopular() {
        List<PeliculaModel> listaPeliculas = Select.from(PeliculaModel.class).where("FLAG_POPULAR = ?", new String[]{"1"}).orderBy("POPULARITY DESC").list();
        if (listaPeliculas == null) {
            listaPeliculas = new ArrayList<>();
        }
        return listaPeliculas;
    }

    public static List<PeliculaModel> getMoviesTopRated() {
        List<PeliculaModel> listaPeliculas = Select.from(PeliculaModel.class).where("FLAG_TOP_RATED = ?", new String[]{"1"}).orderBy("VOTEAVERAGE DESC").list();
        if (listaPeliculas == null) {
            listaPeliculas = new ArrayList<>();
        }
        return listaPeliculas;
    }

    public static List<PeliculaModel> getMoviesUpComing() {
        List<PeliculaModel> listaPeliculas = Select.from(PeliculaModel.class).where("FLAG_UP_COMING = ?", new String[]{"1"}).orderBy("POPULARITY DESC").list();
        if (listaPeliculas == null) {
            listaPeliculas = new ArrayList<>();
        }
        return listaPeliculas;
    }

    public static void saveSeries(ListaSeries listaSeries) {
        for (SerieModel serieModel : listaSeries.results) {
            SerieModel serieModelDB = SugarRecord.findById(SerieModel.class,serieModel.id);
            if (serieModelDB != null) {
                serieModel.flagPopular = serieModelDB.flagPopular;
                serieModel.flagTopRated = serieModelDB.flagTopRated;
            }
        }
        SugarRecord.saveInTx(listaSeries.results);
    }

    public static void saveSeries(String requestType, ListaSeries listaSeries) {
        if (listaSeries.page == 1) {
            List<SerieModel> series;
            switch (requestType) {
                case REQUEST_SERIES_POPULAR:
                    SugarRecord.deleteAll(SerieModel.class,"FLAG_POPULAR = ? AND FLAG_TOP_RATED = ?","1","0");
                    series = getSeriesPopular();
                    if (series != null) {
                        for (SerieModel serieModel: series) {
                            serieModel.flagPopular = false;
                        }
                        SugarRecord.saveInTx(series);
                    }
                    break;
                case REQUEST_SERIES_TOP_RATED:
                    SugarRecord.deleteAll(SerieModel.class,"FLAG_POPULAR = ? AND FLAG_TOP_RATED = ?","0","1");
                    series = getSeriesTopRated();
                    if (series != null) {
                        for (SerieModel serieModel: series) {
                            serieModel.flagTopRated = false;
                        }
                        SugarRecord.saveInTx(series);
                    }
                    break;
            }
        }
        boolean flagPupular = false;
        boolean flagTopRated = false;
        switch (requestType) {
            case REQUEST_SERIES_POPULAR:
                flagPupular = true;
                break;
            case REQUEST_SERIES_TOP_RATED:
                flagTopRated = true;
                break;
        }
        for (SerieModel serieModel : listaSeries.results) {
            SerieModel serieModelDB = SugarRecord.findById(SerieModel.class,serieModel.id);
            if (serieModelDB != null) {
                serieModel.flagPopular = flagPupular || serieModelDB.flagPopular;
                serieModel.flagTopRated = flagTopRated || serieModelDB.flagTopRated;
            } else {
                serieModel.flagPopular = flagPupular;
                serieModel.flagTopRated = flagTopRated;
            }
        }
        SugarRecord.saveInTx(listaSeries.results);
    }

    public static List<SerieModel> searchSeries(String query) {
        return Select.from(SerieModel.class).where("NAME LIKE ?",new String[]{"%"+query+"%"}).list();
    }

    public static List<SerieModel> getSeriesPopular() {
        List<SerieModel> listaSeries = Select.from(SerieModel.class).where("FLAG_POPULAR = ?", new String[]{"1"}).orderBy("POPULARITY DESC").list();
        if (listaSeries == null) {
            listaSeries = new ArrayList<>();
        }
        return listaSeries;
    }

    public static List<SerieModel> getSeriesTopRated() {
        List<SerieModel> listaSeries = Select.from(SerieModel.class).where("FLAG_TOP_RATED = ?", new String[]{"1"}).orderBy("VOTEAVERAGE DESC").list();
        if (listaSeries == null) {
            listaSeries = new ArrayList<>();
        }
        return listaSeries;
    }

}
