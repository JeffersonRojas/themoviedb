package org.themoviedb.utils;

import android.content.res.Resources;
import android.databinding.BindingAdapter;
import android.widget.ImageView;
import android.widget.ScrollView;

import com.squareup.picasso.Picasso;

import org.themoviedb.R;

import static org.themoviedb.requests.ApiClient.BASE_BIG_URL_IMAGE;
import static org.themoviedb.requests.ApiClient.BASE_SMALL_URL_IMAGE;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class DataBindingUtils {
    @BindingAdapter({"bind:posterUrl"})
    public static void loadImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(BASE_SMALL_URL_IMAGE +imageUrl)
                .error(R.drawable.ic_the_movie_db)
                .into(view);
    }

    @BindingAdapter({"bind:bigPosterUrl"})
    public static void loadBigImage(ImageView view, String imageUrl) {
        Picasso.with(view.getContext())
                .load(BASE_BIG_URL_IMAGE +imageUrl)
                .error(R.drawable.ic_the_movie_db)
                .into(view);
    }

    @BindingAdapter({"bind:setPaddingTop"})
    public static void setPaddingTop(ScrollView view, String text) {
        int paddingTop = Resources.getSystem().getDisplayMetrics().heightPixels;
        view.setPadding(0,paddingTop,0,0);
        view.scrollTo(0,paddingTop/5);
    }

}
