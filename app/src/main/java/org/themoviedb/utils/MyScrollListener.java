package org.themoviedb.utils;

import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class MyScrollListener extends RecyclerView.OnScrollListener {

    private ScrollBottomListener listener;

    public MyScrollListener(ScrollBottomListener listener) {
        super();
        this.listener = listener;
    }

    @Override
    public void onScrollStateChanged(RecyclerView recyclerView, int newState) {
        super.onScrollStateChanged(recyclerView, newState);
    }

    @Override
    public void onScrolled(RecyclerView recyclerView, int dx, int dy) {
        super.onScrolled(recyclerView, dx, dy);
        LinearLayoutManager manager = (LinearLayoutManager) recyclerView.getLayoutManager();
        int visibleItemCount = manager.getChildCount();
        int totalItemCount = manager.getItemCount();
        int pastVisibleItems = manager.findFirstVisibleItemPosition();
        if (pastVisibleItems + visibleItemCount >= totalItemCount) {
            listener.onScrollBottom();
        }
    }

    public interface ScrollBottomListener {
        void onScrollBottom();
    }

}
