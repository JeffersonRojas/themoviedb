package org.themoviedb.requests;

import org.themoviedb.models.ListaPeliculas;
import org.themoviedb.models.ListaSeries;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;
import org.themoviedb.utils.SugarUtils;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class ApiResponse {

    public static void searchPelicula(Call<ListaPeliculas> callLista, final OnResponse onResponse) {
        callLista.enqueue(new Callback<ListaPeliculas>() {
            @Override
            public void onResponse(Call<ListaPeliculas> call, Response<ListaPeliculas> response) {
                SugarUtils.savePeliculas(response.body());
                onResponse.onResponsePeliculas(response.body().results);
            }

            @Override
            public void onFailure(Call<ListaPeliculas> call, Throwable t) {
                onResponse.onError(t);
            }

        });
    }

    public static void getListaPeliculas(final String requestType, Call<ListaPeliculas> callLista, final OnResponse onResponse) {
        callLista.enqueue(new Callback<ListaPeliculas>() {
            @Override
            public void onResponse(Call<ListaPeliculas> call, Response<ListaPeliculas> response) {
                ListaPeliculas listaPeliculas = response.body();
                SugarUtils.savePeliculas(requestType,listaPeliculas);
                onResponse.onResponsePeliculas(listaPeliculas.results);
            }

            @Override
            public void onFailure(Call<ListaPeliculas> call, Throwable t) {
                onResponse.onError(t);
            }

        });
    }

    public static void searchSerie(Call<ListaSeries> callLista, final OnResponse onResponse) {
        callLista.enqueue(new Callback<ListaSeries>() {
            @Override
            public void onResponse(Call<ListaSeries> call, Response<ListaSeries> response) {
                SugarUtils.saveSeries(response.body());
                onResponse.onResponseSeries(response.body().results);
            }

            @Override
            public void onFailure(Call<ListaSeries> call, Throwable t) {
                onResponse.onError(t);
            }
        });
    }

    public static void getListaSeries(final String requestType, Call<ListaSeries> callLista, final OnResponse onResponse) {
        callLista.enqueue(new Callback<ListaSeries>() {
            @Override
            public void onResponse(Call<ListaSeries> call, Response<ListaSeries> response) {
                ListaSeries listaSeries = response.body();
                SugarUtils.saveSeries(requestType,listaSeries);
                onResponse.onResponseSeries(listaSeries.results);
            }

            @Override
            public void onFailure(Call<ListaSeries> call, Throwable t) {
                onResponse.onError(t);
            }
        });
    }

    public interface OnResponse {

        void onResponsePeliculas(List<PeliculaModel> listaPeliculas);

        void onResponseSeries(List<SerieModel> liastaSeries);

        void onError(Throwable t);

    }

}
