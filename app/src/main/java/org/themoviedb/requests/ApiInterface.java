package org.themoviedb.requests;

import org.themoviedb.models.DetallesPelicula;
import org.themoviedb.models.ListaPeliculas;
import org.themoviedb.models.ListaSeries;

import retrofit2.Call;
import retrofit2.http.GET;
import retrofit2.http.Path;
import retrofit2.http.Query;

import static org.themoviedb.requests.ApiClient.PELICULAS;
import static org.themoviedb.requests.ApiClient.POPULAR;
import static org.themoviedb.requests.ApiClient.SEARCH_MOVIE;
import static org.themoviedb.requests.ApiClient.SEARCH_SERIE;
import static org.themoviedb.requests.ApiClient.SERIES;
import static org.themoviedb.requests.ApiClient.TOP_RATED;
import static org.themoviedb.requests.ApiClient.UP_COMING;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public interface ApiInterface {

    @GET(SEARCH_MOVIE)
    Call<ListaPeliculas> searchMovie(@Query("api_key") String apiKey, @Query("language") String language, @Query("query") String query, @Query("page") Integer page);

    @GET(PELICULAS+"{id_pelicula}")
    Call<DetallesPelicula> getMovie(@Path("id_pelicula") String idPelicula, @Query("api_key") String apiKey, @Query("language") String language);

    @GET(PELICULAS+POPULAR)
    Call<ListaPeliculas> getPopularMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

    @GET(PELICULAS+TOP_RATED)
    Call<ListaPeliculas> getTopRatedMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

    @GET(PELICULAS+UP_COMING)
    Call<ListaPeliculas> getUpComingMovies(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

    @GET(SEARCH_SERIE)
    Call<ListaSeries> searchSerie(@Query("api_key") String apiKey, @Query("language") String language, @Query("query") String query, @Query("page") Integer page);

    @GET(SERIES+POPULAR)
    Call<ListaSeries> getPopularTv(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

    @GET(SERIES+TOP_RATED)
    Call<ListaSeries> getTopRatedTv(@Query("api_key") String apiKey, @Query("language") String language, @Query("page") Integer page);

}
