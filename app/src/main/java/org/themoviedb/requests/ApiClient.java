package org.themoviedb.requests;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class ApiClient {

    private static final String BASE_URL = "http://api.themoviedb.org/3/";

    public static final String BASE_SMALL_URL_IMAGE = "https://image.tmdb.org/t/p/w92";
    public static final String BASE_BIG_URL_IMAGE = "https://image.tmdb.org/t/p/original";
    public static final String API_KEY = "33b17ab57b26981fc9b738094617084d";

    static final String SEARCH_MOVIE = "search/movie";
    static final String SEARCH_SERIE = "search/tv";
    static final String PELICULAS = "movie/";
    static final String SERIES = "tv/";
    static final String POPULAR = "popular";
    static final String TOP_RATED = "top_rated";
    static final String UP_COMING = "upcoming";

    private static Retrofit retrofit = null;

    public static Retrofit getClient() {
        if (retrofit == null) {
            retrofit = new Retrofit.Builder()
                    .baseUrl(BASE_URL)
                    .addConverterFactory(GsonConverterFactory.create())
                    .build();
        }
        return retrofit;
    }

}
