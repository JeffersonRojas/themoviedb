package org.themoviedb.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.themoviedb.R;
import org.themoviedb.adapters.RecyclerAdapter;
import org.themoviedb.models.ListaPeliculas;
import org.themoviedb.models.ListaSeries;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;
import org.themoviedb.requests.ApiClient;
import org.themoviedb.requests.ApiInterface;
import org.themoviedb.requests.ApiResponse;
import org.themoviedb.utils.MyScrollListener;
import org.themoviedb.utils.SugarUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static org.themoviedb.requests.ApiClient.API_KEY;

/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class RecyclerFragmentList extends Fragment implements SwipeRefreshLayout.OnRefreshListener, MyScrollListener.ScrollBottomListener, ApiResponse.OnResponse {

    private static final String REQUEST_TYPE = "RecyclerFragmentList$RequestType";

    public static final String REQUEST_MOVIE_POPULAR = "RequestType&Movie&Popular";
    public static final String REQUEST_MOVIE_TOP_RATED = "RequestType&Movie&TopRated";
    public static final String REQUEST_MOVIE_UP_COMING = "RequestType&Movie&UpComing";
    public static final String REQUEST_SERIES_POPULAR = "RequestType&Serie&Popular";
    public static final String REQUEST_SERIES_TOP_RATED = "RequestType&Serie&TopRated";

    SwipeRefreshLayout swipeRefreshLayout;
    List<PeliculaModel> listaPeliculas;
    List<SerieModel> listaSeries;
    String requestType;
    RecyclerAdapter recyclerAdapter;

    @BindView(R.id.recycler_view)
    RecyclerView recyclerView;

    public static RecyclerFragmentList createFragment(String requestType) {
        RecyclerFragmentList recyclerFragmentList = new RecyclerFragmentList();
        Bundle bundle = new Bundle();
        bundle.putString(REQUEST_TYPE,requestType);
        recyclerFragmentList.setArguments(bundle);
        return recyclerFragmentList;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        swipeRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_recycler_view, container, false);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
        ButterKnife.bind(this,swipeRefreshLayout);
        listaPeliculas = new ArrayList<>();
        listaSeries = new ArrayList<>();
        requestType = getArguments().getString(REQUEST_TYPE);
        recyclerAdapter = new RecyclerAdapter();
        switch (requestType) {
            case REQUEST_MOVIE_POPULAR:
                listaPeliculas = SugarUtils.getMoviesPopular();
                recyclerAdapter.setListPeliculas(listaPeliculas);
                break;
            case REQUEST_MOVIE_TOP_RATED:
                listaPeliculas = SugarUtils.getMoviesTopRated();
                recyclerAdapter.setListPeliculas(listaPeliculas);
                break;
            case REQUEST_MOVIE_UP_COMING:
                listaPeliculas = SugarUtils.getMoviesUpComing();
                recyclerAdapter.setListPeliculas(listaPeliculas);
                break;
            case REQUEST_SERIES_POPULAR:
                listaSeries = SugarUtils.getSeriesPopular();
                recyclerAdapter.setListSeries(listaSeries);
                break;
            case REQUEST_SERIES_TOP_RATED:
                listaSeries = SugarUtils.getSeriesTopRated();
                recyclerAdapter.setListSeries(listaSeries);
                break;
        }
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.addOnScrollListener(new MyScrollListener(this));
        recyclerView.setAdapter(recyclerAdapter);
        if (listaSeries.size() == 0 && listaPeliculas.size() == 0) {
            requestList();
        }
        return swipeRefreshLayout;
    }



    private void requestList() {
        swipeRefreshLayout.setRefreshing(true);
        String keyLanguage = Locale.getDefault().getLanguage();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ListaPeliculas> callPeliculas;
        Call<ListaSeries> callSeries;
        switch (requestType) {
            case REQUEST_MOVIE_POPULAR:
                callPeliculas = apiService.getPopularMovies(API_KEY, keyLanguage, (listaPeliculas.size() / 20) + 1);
                ApiResponse.getListaPeliculas(requestType,callPeliculas,this);
                break;
            case REQUEST_MOVIE_TOP_RATED:
                callPeliculas = apiService.getTopRatedMovies(API_KEY, keyLanguage, (listaPeliculas.size() / 20) + 1);
                ApiResponse.getListaPeliculas(requestType,callPeliculas,this);
                break;
            case REQUEST_MOVIE_UP_COMING:
                callPeliculas = apiService.getUpComingMovies(API_KEY, keyLanguage, (listaPeliculas.size() / 20) + 1);
                ApiResponse.getListaPeliculas(requestType,callPeliculas,this);
                break;
            case REQUEST_SERIES_POPULAR:
                callSeries = apiService.getPopularTv(API_KEY, keyLanguage, (listaSeries.size() / 20) + 1);
                ApiResponse.getListaSeries(requestType,callSeries,this);
                break;
            case REQUEST_SERIES_TOP_RATED:
                callSeries = apiService.getTopRatedTv(API_KEY, keyLanguage, (listaSeries.size() / 20) + 1);
                ApiResponse.getListaSeries(requestType,callSeries,this);
                break;
        }
    }

    @Override
    public void onRefresh() {
        listaPeliculas = new ArrayList<>();
        listaSeries = new ArrayList<>();
        requestList();
    }

    @Override
    public void onScrollBottom() {
        if (swipeRefreshLayout.isRefreshing()) {
            return;
        }
        requestList();
    }

    @Override
    public void onResponsePeliculas(List<PeliculaModel> listaPeliculas) {
        this.listaPeliculas.addAll(listaPeliculas);
        recyclerAdapter.setListPeliculas(this.listaPeliculas);
        recyclerAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onResponseSeries(List<SerieModel> listaSeries) {
        this.listaSeries.addAll(listaSeries);
        recyclerAdapter.setListSeries(this.listaSeries);
        recyclerAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onError(Throwable t) {
        swipeRefreshLayout.setRefreshing(false);
        switch (requestType) {
            case REQUEST_MOVIE_POPULAR:
                recyclerAdapter.setListPeliculas(SugarUtils.getMoviesPopular());
                break;
            case REQUEST_MOVIE_TOP_RATED:
                recyclerAdapter.setListPeliculas(SugarUtils.getMoviesTopRated());
                break;
            case REQUEST_MOVIE_UP_COMING:
                recyclerAdapter.setListPeliculas(SugarUtils.getMoviesUpComing());
                break;
            case REQUEST_SERIES_POPULAR:
                recyclerAdapter.setListSeries(SugarUtils.getSeriesPopular());
                break;
            case REQUEST_SERIES_TOP_RATED:
                recyclerAdapter.setListSeries(SugarUtils.getSeriesTopRated());
                break;
        }
        recyclerAdapter.notifyDataSetChanged();
    }

}