package org.themoviedb.fragments;

import android.content.Context;
import android.content.Intent;
import android.support.v7.widget.RecyclerView;
import android.widget.ImageView;

import org.themoviedb.R;
import org.themoviedb.activities.DetalleItem;
import org.themoviedb.databinding.HolderPeliculaBinding;
import org.themoviedb.databinding.HolderSerieBinding;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

/**
 * Created by Jefferson Rojas on 19/03/2017.
 */

public class ItemViewHolder extends RecyclerView.ViewHolder {

    private HolderPeliculaBinding holderPeliculaBinding;
    private HolderSerieBinding holderSerieBinding;

    @BindView(R.id.iv_poster)
    ImageView ivPoster;

    public ItemViewHolder(HolderSerieBinding holderSerieBinding) {
        super(holderSerieBinding.getRoot());
        ButterKnife.bind(this,holderSerieBinding.getRoot());
        this.holderSerieBinding = holderSerieBinding;
    }

    public ItemViewHolder(HolderPeliculaBinding holderPeliculaBinding) {
        super(holderPeliculaBinding.getRoot());
        ButterKnife.bind(this,holderPeliculaBinding.getRoot());
        this.holderPeliculaBinding = holderPeliculaBinding;
    }

    public void setModel(SerieModel model) {
        holderSerieBinding.setModel(model);
    }

    public void setModel(PeliculaModel model) {
        holderPeliculaBinding.setModel(model);
    }

    @OnClick(R.id.card_view)
    void onClickItem() {
        if (holderPeliculaBinding == null && holderSerieBinding != null) {
            SerieModel serieModel = holderSerieBinding.getModel();
            Context context = holderSerieBinding.getRoot().getContext();
            Intent intent = new Intent(context, DetalleItem.class);
            intent.putExtra(DetalleItem.EXTRA_ITEM_TYPE,DetalleItem.ITEM_TYPE_SERIE);
            intent.putExtra(DetalleItem.EXTRA_ITEM_ID,serieModel.id);
            context.startActivity(intent);
        } else if (holderSerieBinding == null && holderPeliculaBinding != null) {
            PeliculaModel peliculaModel = holderPeliculaBinding.getModel();
            Context context = holderPeliculaBinding.getRoot().getContext();
            Intent intent = new Intent(context, DetalleItem.class);
            intent.putExtra(DetalleItem.EXTRA_ITEM_TYPE,DetalleItem.ITEM_TYPE_PELICULA);
            intent.putExtra(DetalleItem.EXTRA_ITEM_ID,peliculaModel.id);
            context.startActivity(intent);
        }
    }

}
