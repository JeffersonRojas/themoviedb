package org.themoviedb.fragments;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.widget.SwipeRefreshLayout;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.themoviedb.R;
import org.themoviedb.adapters.RecyclerAdapter;
import org.themoviedb.models.ListaPeliculas;
import org.themoviedb.models.ListaSeries;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;
import org.themoviedb.requests.ApiClient;
import org.themoviedb.requests.ApiInterface;
import org.themoviedb.requests.ApiResponse;
import org.themoviedb.utils.MyScrollListener;
import org.themoviedb.utils.SugarUtils;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;

import static org.themoviedb.requests.ApiClient.API_KEY;

/**
 * Created by Jefferson Rojas on 21/03/2017.
 */

public class RecyclerFragmentSearch extends Fragment implements SwipeRefreshLayout.OnRefreshListener, MyScrollListener.ScrollBottomListener, ApiResponse.OnResponse {

    private static final String SEARCH_TYPE = "RecyclerFragmentSearch$SearchType";

    public static final String SEARCH_MOVIE = "RecyclerFragmentSearch$SearchType&Movie";
    public static final String SEARCH_SERIE = "RecyclerFragmentSearch$SearchType&Serie";

    SwipeRefreshLayout swipeRefreshLayout;
    List<PeliculaModel> listaPeliculas;
    List<SerieModel> listaSeries;
    String searchType;
    String query;
    MyScrollListener myScrollListener;
    RecyclerAdapter recyclerAdapter;

    @BindView(R.id.recycler_view)
    public RecyclerView recyclerView;

    public static RecyclerFragmentSearch createFragment(String searchType) {
        RecyclerFragmentSearch recyclerFragmentSearch = new RecyclerFragmentSearch();
        Bundle bundle = new Bundle();
        bundle.putString(SEARCH_TYPE,searchType);
        recyclerFragmentSearch.setArguments(bundle);
        return recyclerFragmentSearch;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        swipeRefreshLayout = (SwipeRefreshLayout) inflater.inflate(R.layout.fragment_recycler_view, container, false);
        swipeRefreshLayout.setColorSchemeResources(R.color.colorAccent,R.color.colorPrimary);
        swipeRefreshLayout.setOnRefreshListener(this);
        ButterKnife.bind(this,swipeRefreshLayout);
        listaPeliculas = new ArrayList<>();
        listaSeries = new ArrayList<>();
        searchType = getArguments().getString(SEARCH_TYPE);
        recyclerAdapter = new RecyclerAdapter();
        myScrollListener = new MyScrollListener(this);
        recyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        recyclerView.setAdapter(recyclerAdapter);
        return swipeRefreshLayout;
    }

    public void search(String query) {
        recyclerView.removeOnScrollListener(myScrollListener);
        listaPeliculas = new ArrayList<>();
        listaSeries = new ArrayList<>();
        this.query = query;
        if (query.isEmpty()) {
            switch (searchType) {
                case SEARCH_MOVIE:
                    recyclerAdapter.setListPeliculas(listaPeliculas);
                    break;
                case SEARCH_SERIE:
                    recyclerAdapter.setListSeries(listaSeries);
                    break;
            }
            recyclerAdapter.notifyDataSetChanged();
            return;
        }
        swipeRefreshLayout.setRefreshing(true);
        String keyLanguage = Locale.getDefault().getLanguage();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ListaPeliculas> callPeliculas;
        Call<ListaSeries> callSeries;
        switch (searchType) {
            case SEARCH_MOVIE:
                callPeliculas = apiService.searchMovie(API_KEY, keyLanguage, query, (listaPeliculas.size() / 20) + 1);
                ApiResponse.searchPelicula(callPeliculas,this);
                break;
            case SEARCH_SERIE:
                callSeries = apiService.searchSerie(API_KEY, keyLanguage, query, (listaSeries.size() / 20) + 1);
                ApiResponse.searchSerie(callSeries,this);
                break;
        }
    }

    @Override
    public void onRefresh() {
        swipeRefreshLayout.setRefreshing(false);
    }

    @Override
    public void onScrollBottom() {
        if (swipeRefreshLayout.isRefreshing()) {
            return;
        }
        swipeRefreshLayout.setRefreshing(true);
        String keyLanguage = Locale.getDefault().getLanguage();
        ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
        Call<ListaPeliculas> callPeliculas;
        Call<ListaSeries> callSeries;
        switch (searchType) {
            case SEARCH_MOVIE:
                callPeliculas = apiService.searchMovie(API_KEY, keyLanguage, query, (listaPeliculas.size() / 20) + 1);
                ApiResponse.searchPelicula(callPeliculas,this);
                break;
            case SEARCH_SERIE:
                callSeries = apiService.searchSerie(API_KEY, keyLanguage, query, (listaSeries.size() / 20) + 1);
                ApiResponse.searchSerie(callSeries,this);
                break;
        }
    }

    @Override
    public void onResponsePeliculas(List<PeliculaModel> listaPeliculas) {
        this.listaPeliculas.addAll(listaPeliculas);
        recyclerAdapter.setListPeliculas(this.listaPeliculas);
        recyclerAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.addOnScrollListener(myScrollListener);
    }

    @Override
    public void onResponseSeries(List<SerieModel> listaSeries) {
        this.listaSeries.addAll(listaSeries);
        recyclerAdapter.setListSeries(this.listaSeries);
        recyclerAdapter.notifyDataSetChanged();
        swipeRefreshLayout.setRefreshing(false);
        recyclerView.addOnScrollListener(myScrollListener);
    }

    @Override
    public void onError(Throwable t) {
        switch (searchType) {
            case SEARCH_MOVIE:
                listaPeliculas = SugarUtils.searchPeliculas(query);
                recyclerAdapter.setListPeliculas(listaPeliculas);
                break;
            case SEARCH_SERIE:
                listaSeries = SugarUtils.searchSeries(query);
                recyclerAdapter.setListSeries(listaSeries);
                break;
        }
        swipeRefreshLayout.setRefreshing(false);
        recyclerAdapter.notifyDataSetChanged();
    }

}