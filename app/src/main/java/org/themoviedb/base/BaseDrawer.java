package org.themoviedb.base;

import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.support.annotation.LayoutRes;
import android.support.annotation.NonNull;
import android.support.design.widget.CoordinatorLayout;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;

import org.themoviedb.R;
import org.themoviedb.activities.Buscar;
import org.themoviedb.activities.Peliculas;
import org.themoviedb.activities.Series;
import org.themoviedb.databinding.ToolbarBinding;

import butterknife.BindView;
import butterknife.ButterKnife;

/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class BaseDrawer extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener, DrawerLayout.DrawerListener {

    CoordinatorLayout coordinatorLayout;
    ToolbarBinding toolbarBinding;

    @BindView(R.id.drawer_layout)
    DrawerLayout drawerLayout;
    @BindView(R.id.nav_view)
    NavigationView navigationView;
    @BindView(R.id.toolbar)
    Toolbar toolbar;

    @Override
    public void setContentView(@LayoutRes int layoutResID) {
        super.setContentView(R.layout.base_drawer_activity);
        inflateLayoutChildActivity(layoutResID);
        ButterKnife.bind(this);
        configToolbar();
        addDrawerToggle();
        setDrawerListeners();

    }

    private void configToolbar() {
        toolbarBinding = DataBindingUtil.bind(findViewById(R.id.app_bar_layout));
        setSupportActionBar(toolbar);
    }

    private void inflateLayoutChildActivity(@LayoutRes int layoutResID) {
        coordinatorLayout = (CoordinatorLayout) findViewById(R.id.coordinator);
        LayoutInflater inflater = (LayoutInflater) this.getSystemService(LAYOUT_INFLATER_SERVICE);
        View activityView = inflater.inflate(layoutResID, coordinatorLayout, false);
        coordinatorLayout.addView(activityView,0);
    }

    private void addDrawerToggle() {
        ActionBarDrawerToggle drawerToggle = new ActionBarDrawerToggle(this, drawerLayout, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawerLayout.addDrawerListener(drawerToggle);
        drawerToggle.syncState();
    }

    private void setDrawerListeners() {
        navigationView.setNavigationItemSelectedListener(this);
        drawerLayout.addDrawerListener(this);
    }

    public void startActivity(Class activityClass) {
        startActivity(new Intent(this,activityClass));
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (this instanceof Peliculas) {
            navigationView.getMenu().findItem(R.id.nav_peliculas).setChecked(true);
            toolbarBinding.setShowTabs(true);
        } else if (this instanceof Series) {
            navigationView.getMenu().findItem(R.id.nav_series).setChecked(true);
            toolbarBinding.setShowTabs(true);
        } else if (this instanceof Buscar) {
            navigationView.getMenu().findItem(R.id.nav_buscar).setChecked(true);
            toolbarBinding.setShowTabs(true);
        }
    }

    @Override
    public boolean onNavigationItemSelected(@NonNull MenuItem item) {
        switch (item.getItemId()) {
            case R.id.nav_peliculas:
                drawerLayout.setTag(Peliculas.class);
                break;
            case R.id.nav_series:
                drawerLayout.setTag(Series.class);
                break;
            case R.id.nav_buscar:
                drawerLayout.setTag(Buscar.class);
                break;
        }
        drawerLayout.closeDrawer(GravityCompat.START);
        return true;
    }

    @Override
    public void onBackPressed() {
        if (drawerLayout.isDrawerOpen(GravityCompat.START)) {
            drawerLayout.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }

    @Override
    public void onDrawerSlide(View drawerView, float slideOffset) {

    }

    @Override
    public void onDrawerOpened(View drawerView) {

    }

    @Override
    public void onDrawerClosed(View drawerView) {
        if (drawerLayout.getTag() instanceof Class) {
            startActivity((Class) drawerLayout.getTag());
            drawerLayout.setTag(null);
        }
    }

    @Override
    public void onDrawerStateChanged(int newState) {

    }

}
