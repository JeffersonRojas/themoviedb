package org.themoviedb.base;

import com.orm.SugarContext;

/**
 * Created by Jefferson Rojas on 20/03/2017.
 */

public class MyApplication extends android.app.Application {

    @Override
    public void onCreate() {
        super.onCreate();
        SugarContext.init(getApplicationContext());
    }

}
