package org.themoviedb.activities;

import android.content.Intent;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;


/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class Splash extends AppCompatActivity implements Runnable {

    private static final int DELAY_SPLASH = 1000;

    @Override
    protected void onResume() {
        super.onResume();
        new Handler().postDelayed(this,DELAY_SPLASH);
    }

    @Override
    public void run() {
        startActivity(new Intent(this,Peliculas.class));
        finish();
    }
}
