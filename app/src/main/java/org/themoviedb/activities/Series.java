package org.themoviedb.activities;

import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.ViewPager;

import org.themoviedb.R;
import org.themoviedb.adapters.PagerAdapterList;
import org.themoviedb.base.BaseDrawer;
import org.themoviedb.fragments.RecyclerFragmentList;

import butterknife.BindView;

/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class Series extends BaseDrawer {

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        PagerAdapterList pagerAdapterList = new PagerAdapterList(getSupportFragmentManager());
        pagerAdapterList.addFragment(RecyclerFragmentList.createFragment(RecyclerFragmentList.REQUEST_SERIES_POPULAR), getString(R.string.popular));
        pagerAdapterList.addFragment(RecyclerFragmentList.createFragment(RecyclerFragmentList.REQUEST_SERIES_TOP_RATED), getString(R.string.toprated));
        viewPager.setOffscreenPageLimit(pagerAdapterList.getCount());
        viewPager.setAdapter(pagerAdapterList);
        tabLayout.setupWithViewPager(viewPager);
    }

}
