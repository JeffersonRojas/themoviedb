package org.themoviedb.activities;

import android.app.SearchManager;
import android.os.Bundle;
import android.support.design.widget.TabLayout;
import android.support.v4.view.MenuItemCompat;
import android.support.v4.view.ViewPager;
import android.support.v7.widget.SearchView;
import android.view.Menu;

import org.themoviedb.R;
import org.themoviedb.adapters.PagerAdapterSearch;
import org.themoviedb.base.BaseDrawer;
import org.themoviedb.fragments.RecyclerFragmentSearch;

import java.util.List;

import butterknife.BindView;

/**
 * Created by Jefferson Rojas on 18/03/2017.
 */

public class Buscar extends BaseDrawer implements SearchView.OnQueryTextListener {

    PagerAdapterSearch pagerAdapterSearch;

    @BindView(R.id.viewPager)
    ViewPager viewPager;
    @BindView(R.id.tabLayout)
    TabLayout tabLayout;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_pager);
        pagerAdapterSearch = new PagerAdapterSearch(getSupportFragmentManager());
        pagerAdapterSearch.addFragment(RecyclerFragmentSearch.createFragment(RecyclerFragmentSearch.SEARCH_MOVIE),getString(R.string.peliculas));
        pagerAdapterSearch.addFragment(RecyclerFragmentSearch.createFragment(RecyclerFragmentSearch.SEARCH_SERIE),getString(R.string.series));
        viewPager.setOffscreenPageLimit(pagerAdapterSearch.getCount());
        viewPager.setAdapter(pagerAdapterSearch);
        tabLayout.setupWithViewPager(viewPager);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.search, menu);
        final SearchView searchView = (SearchView) MenuItemCompat.getActionView(menu.findItem(R.id.action_search));
        SearchManager searchManager = (SearchManager) getSystemService(SEARCH_SERVICE);
        searchView.setSearchableInfo(searchManager.getSearchableInfo(getComponentName()));
        searchView.setId(R.id.mask);
        searchView.setOnQueryTextListener(this);
        return true;
    }

    @Override
    public boolean onQueryTextSubmit(String query) {
        List<RecyclerFragmentSearch> fragmentList = pagerAdapterSearch.getFragmentList();
        for (RecyclerFragmentSearch fragmentSearch : fragmentList) {
            if (fragmentSearch.recyclerView != null) {
                fragmentSearch.search(query);
            }
        }
        return false;
    }

    @Override
    public boolean onQueryTextChange(String newText) {
        List<RecyclerFragmentSearch> fragmentList = pagerAdapterSearch.getFragmentList();
        for (RecyclerFragmentSearch fragmentSearch : fragmentList) {
            if (fragmentSearch.recyclerView != null) {
                fragmentSearch.search(newText);
            }
        }
        return true;
    }

}
