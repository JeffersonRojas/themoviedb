package org.themoviedb.activities;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewTreeObserver;
import android.widget.ScrollView;

import com.orm.SugarRecord;

import org.themoviedb.R;
import org.themoviedb.databinding.ActivityDetallePeliculaBinding;
import org.themoviedb.databinding.ActivityDetalleSerieBinding;
import org.themoviedb.models.DetallesPelicula;
import org.themoviedb.models.PeliculaModel;
import org.themoviedb.models.SerieModel;
import org.themoviedb.requests.ApiClient;
import org.themoviedb.requests.ApiInterface;

import java.util.Locale;

import butterknife.BindView;
import butterknife.ButterKnife;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

/**
 * Created by Jefferson Rojas on 22/03/2017.
 */

public class DetalleItem extends AppCompatActivity implements ViewTreeObserver.OnScrollChangedListener {

    public static final String EXTRA_ITEM_ID = "DetalleItem$ID";
    public static final String EXTRA_ITEM_TYPE = "DetalleItem&ItemType";
    public static final String ITEM_TYPE_PELICULA = "DetalleItem&Pelicula";
    public static final String ITEM_TYPE_SERIE = "DetalleItem&Serie";

    ActivityDetallePeliculaBinding peliculaBinding;

    ActivityDetalleSerieBinding serieBinding;

    @BindView(R.id.toolbar_item)
    Toolbar toolbar;
    @BindView(R.id.scroll_view)
    ScrollView scrollView;
    @BindView(R.id.mask)
    View mask;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        String itemType = getIntent().getStringExtra(EXTRA_ITEM_TYPE);
        Long idItem = getIntent().getLongExtra(EXTRA_ITEM_ID,0);
        if (itemType.equals(ITEM_TYPE_PELICULA)) {
            peliculaBinding = DataBindingUtil.setContentView(this,R.layout.activity_detalle_pelicula);
            PeliculaModel peliculaModel = SugarRecord.findById(PeliculaModel.class,idItem);
            peliculaBinding.setModel(peliculaModel);
            ButterKnife.bind(this);
            scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
            setSupportActionBar(toolbar);
            setTitle(peliculaModel.title);
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            ApiInterface apiService = ApiClient.getClient().create(ApiInterface.class);
            Call<DetallesPelicula> call = apiService.getMovie(String.valueOf(peliculaModel.id), ApiClient.API_KEY, Locale.getDefault().getLanguage());
            call.enqueue(new Callback<DetallesPelicula>() {
                @Override
                public void onResponse(Call<DetallesPelicula> call, Response<DetallesPelicula> response) {
                    peliculaBinding.setDetalle(response.body());
                }

                @Override
                public void onFailure(Call<DetallesPelicula> call, Throwable t) {

                }
            });
        } else if (itemType.equals(ITEM_TYPE_SERIE)) {
            serieBinding = DataBindingUtil.setContentView(this,R.layout.activity_detalle_serie);
            SerieModel serieModel = SugarRecord.findById(SerieModel.class,idItem);
            serieBinding.setModel(serieModel);
            ButterKnife.bind(this);
            scrollView.getViewTreeObserver().addOnScrollChangedListener(this);
            setSupportActionBar(toolbar);
            setTitle(serieModel.name);
            if (getSupportActionBar() != null)
                getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        }

    }

    @Override
    public void onScrollChanged() {
        float scrollY = scrollView.getScrollY();
        float padding = scrollView.getPaddingTop();
        float alpha = 1 - ((padding - scrollY) / padding);
        toolbar.setAlpha(alpha);
        mask.setAlpha(alpha);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        int id = item.getItemId();
        if (id == android.R.id.home) {
            onBackPressed();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

}
